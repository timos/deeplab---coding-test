import torch.nn.functional as F
import torch.nn as nn
import torch.optim as optim
import copy
import torch

class MLP(nn.Module):
    
    """
            Defines a simple neural network used for our text classification task
        
    """
    def __init__(self,input_dimension = 768, output_dimensions=100, layers = [100]):
        super(MLP, self).__init__()
        
        layers.insert(0, input_dimension)
        layers.append(output_dimensions)
        self.layers = nn.ModuleList()
        
        for idx in range(len(layers)-1):
            self.layers.append(nn.Linear(layers[idx],layers[idx+1]))
        self.apply(self.init_weights)

    def forward(self, vector):

        for layer in self.layers[:-1]:
            vector = layer(vector)
            vector = nn.functional.leaky_relu(vector)
        logits = self.layers[-1](vector)

        return logits

    def init_weights(self,m):
        if type(m) == nn.Linear:
            torch.nn.init.xavier_uniform_(m.weight)
            m.bias.data.fill_(0.01)