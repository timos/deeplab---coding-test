import numpy as np
import pandas as pd
import torch
import nltk
nltk.download('punkt')

from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import RandomOverSampler
from sklearn.utils import class_weight
from sklearn.metrics import f1_score
from InferSent.models import InferSent
from itertools import chain
from pytorch_pretrained_bert import BertTokenizer, BertModel
from sklearn.decomposition import PCA

def pca_transform(x,dimensions_to_keep):
    """
    Finds the most important components of a dataset 
    using PCA.
        -------
        Input
            x: nd.array (example_num,dimensions)
            dimensions_to_keep: int 
        -------
        Output
            pca: sklearn.decomposition class to perform transformation of dataset 

    """
    pca = PCA(n_components=dimensions_to_keep)
    pca.fit(x)
    return pca

def load_dataframe(filename,read_from_file=True,embed_path=None):
    """
    Load the data from the file. 
        
        ---------    
        Input
            filename: Filepath to read data
            read_from_file: If set True then the data will be read from embed_path. Used to pre-process and sava the data once.
            embed_path: Read to store the pre-processed data set. If read_from_file == True it loads the pre-processed data from embed_path
        Output
            df : pandas.Dataframe containing the data

    """

    """
    Loads the data from file and returns them pre-processed as a pandas.DataFrame
        
        Input: 
        -------
            filename: Relative path to the file containing the data
        Output:
        -------
            df: pandas.Dataframe containing the file in a tabular form
            infersent: The neural nework used to extract features from a sentence 
    """
    # Pre-process text for training a deep learning model
    V = 2
    MODEL_PATH = 'encoder/infersent%s.pkl' % V
    params_model = {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                    'pool_type': 'max', 'dpout_model': 0.0, 'version': V}
    infersent = InferSent(params_model)
    infersent.load_state_dict(torch.load(MODEL_PATH))
    W2V_PATH = 'fastText/crawl-300d-2M.vec'
    infersent.set_w2v_path(W2V_PATH)
   

    line_str = []
    non_valid_movies = 0 
    with open(filename) as fp:  
        next(fp) # Skip first line of code
        line = fp.readline()
        while line:
            line = fp.readline()
            # Some non-correctly lines; just ignore them 
            if len(line.split(';')) != 3:
                non_valid_movies+=1
                continue
            line_list = line.split(';')[1:3]
            line_list[1] = line_list[1][:-1]
            line_str.append(line_list)
    infersent.build_vocab(list(chain.from_iterable(line_str)), tokenize=True)
    
    if read_from_file:
        df = pd.read_pickle(embed_path)
    else:
        df = pd.DataFrame(line_str)
        df.columns = ['title','cluster']
        df = df.drop_duplicates(['title','cluster'])
        embeddings = infersent.encode(list(df.title.values), tokenize=True)
        df = df.drop('title',axis=1)
        df = df.reset_index(drop=True)
        df1 = pd.DataFrame.from_records(embeddings)
        df = pd.concat([df,df1],axis=1)
        df.cluster = pd.to_numeric(df.cluster)
        df.to_pickle(embed_path)
    return df,infersent

def correct_missing_clusters(df,column_name):
    cluster_unique = df[column_name].unique()
    clusters = np.arange(len(cluster_unique))
    maping = dict(zip(cluster_unique, clusters))
    return df.cluster.map(maping)

def return_dataset(df,shuffle_data=True,stratify=False,test_percentage = 0.2, val_percentage=0.25):
    
    """
    Returns the dataset in numpy
    -------
        Input:
            df: pandas.Dataframe containing the data
            shuffle_data: whether to shuffle the data or not
            stratify: When spliting the data, if set true it splits the data respecting their distribution in df
        Output:
            x_train,y_train: nd_arrays with training features and corresponding classes
            x_val,y_val: nd_arrays with validationg features and corresponding classes
            x_test,y_test: nd_arrays with testing features and corresponding classes
    """

    y = df.cluster.values
    df1 = df.drop(columns = ['cluster'])
    x = df1.values
    embed_dim = x.shape[1]
    datapoints = x.shape[0]
    y = df.cluster.values
    if stratify:
        x_train, x_test, y_train, y_test = train_test_split(x,y,test_size = test_percentage,shuffle=shuffle_data,stratify=y)
        x_train,x_val,y_train,y_val = train_test_split(x_train,y_train,test_size = val_percentage,shuffle=shuffle_data,stratify=y_train)
    else:
        x_train, x_test, y_train, y_test = train_test_split(x,y,test_size = test_percentage ,shuffle=shuffle_data)
        x_train,x_val,y_train,y_val = train_test_split(x_train,y_train,test_size = val_percentage,shuffle=shuffle_data)
    return x_train,y_train,x_val,y_val,x_test,y_test, embed_dim, datapoints