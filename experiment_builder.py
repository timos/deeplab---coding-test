import numpy as np
import pandas as pd
import torch
import torch.nn as nn 
import copy
import matplotlib.pyplot as plt
from sklearn.metrics import f1_score

class experiment_builder (object):

    """
    Class which defines the whole training, testing and evaluation process
    
    
        Input
        -------
            x_train,               # nd_array (train_size .features ) 
            y_train,               # nd_array (train_size,1) 
            x_val,                 # nd_array (test_size, features ) 
            y_val,                 # nd_array (train_size, 1) 
            model,                 # architecture to be used 
            optim,                 # Optimizer used for updating network params
            feature_extractor,     # Network used to make the feature extractiong from future sentences
            scheduler=None,        # Scheduler used to control the learning reate
            class_weights = None,  # Weights for classes. Better be used when classes are higly unbalanced
            training_epochs = 15,  # Training epochs
            batch_size = 128,       
            l2 =0.0,
            embed_dim,             # Dimension of the word embedding
            lr=1e-4,
            """

    def __init__(self,  x_train, # Features of our correspoinf
                        y_train, 
                        x_val, 
                        y_val,
                        model,
                        optim,
                        embed_dim, 
                        feature_extractor, 
                        scheduler=None,
                        class_weights = None,
                        training_epochs = 15,
                        batch_size = 128,
                        l2 =0.0,
                        pca_transform = None,
                        lr=1e-4,
                        use_cuda=False,
            ):        
        
        self.dtype = torch.FloatTensor
        self.training_epochs = training_epochs
        self.batch_size = batch_size
        self.model = model
        self.embed_dim = embed_dim
        self.best_model = None
        self.best_val_model_acc = 0
        self.best_val_model = None
        self.feature_extractor = feature_extractor
        if class_weights is not None:
            self.loss = nn.CrossEntropyLoss(weight=class_weights)
        else:
            self.loss = nn.CrossEntropyLoss()

        if use_cuda:
            self.device = torch.device('cuda')
        else:
            self.device = torch.device('cpu')
        self.x_train = torch.from_numpy(x_train).float()
        self.y_train = torch.from_numpy(y_train).long()
        self.x_val = torch.from_numpy(x_val).float()
        self.y_val = torch.from_numpy(y_val).long()
        self.model.to(self.device)
        self.optim = optim
        self.scheduler = scheduler
        self.pca_transform = pca_transform
        
    def minibatch(self,*tensors, **kwargs):
        """
            Lazy evaluation of batch size of given arguments. Used to fetch corresponding batches
        """

        batch_size = kwargs.get('batch_size', 128)

        if len(tensors) == 1:
            tensor = tensors[0]
            for i in range(0, len(tensor), batch_size):
                yield tensor[i:i + batch_size]
        else:
            for i in range(0, len(tensors[0]), batch_size):
                yield tuple(x[i:i + batch_size] for x in tensors)
        
    def fit_model(self):
        """
            Function to train the model on the training examples 

        """
        stats = {'train_loss': [], 'train_f1': [], 'valid_loss':[], 'train_f1': [],  'curr_epoch': []}
        
        for epoch_idx in range(self.training_epochs):
            self.model.train()
            epoch_stats = {'train_loss': [], 'train_f1': [], 'valid_loss':[], 'train_f1': []}
            
            for i,(x_batch,y_batch) in enumerate(self.minibatch(self.x_train,self.y_train,batch_size=self.batch_size)):
                loss,f1_score = self.run_train_iter(x_batch,y_batch)
              
                epoch_stats["train_loss"].append(loss)         # add current iter loss to the train loss list
                epoch_stats["train_f1"].append(f1_score)      # add current iter acc to the train acc list
            self.model.eval()
            for x_batch,y_batch in self.minibatch(self.x_val,self.y_val,batch_size=self.batch_size):
                loss,f1_score = self.run_valid_iter(x_batch,y_batch)
                epoch_stats["valid_loss"].append(loss)         # add current iter loss to the train loss list
                epoch_stats["train_f1"].append(f1_score)      # add curr
            
            val_mean_f1_score = np.mean(epoch_stats['train_f1'])
            f1_score
            #Store the model with the highest validation f1 score  

            if val_mean_f1_score > self.best_val_model_acc:  
                self.best_val_model_acc = val_mean_f1_score  
                self.best_val_model = copy.deepcopy(self.model) 
            for key, value in epoch_stats.items():
                stats[key].append(np.mean(value))  # get mean of all metrics of current epoch metrics dict, to get them ready for storage and output on the terminal.

            stats['curr_epoch'].append(epoch_idx)
            if epoch_idx % 10 == 0:
                print("Epoch {} - train_loss {} train_f1 {} val_loss {} val_f1 {}".format(epoch_idx,stats['train_loss'][-1],stats['train_f1'][-1],stats['valid_loss'][-1],stats['train_f1'][-1]))
        self.stats = stats
        
    def test_model(self,x_test,y_test):
        """
        Test the model on a given test data 

            Input
            -------
                x_test: nd.array (test_size,features)
                y_test: nd.array (test_size,)

        """
        test_stats = {"test_f1": [], "test_loss": []}
        x_test_vector = torch.from_numpy(x_test).float()
        y_test_vector = torch.from_numpy(y_test).long()
        for x_batch,y_batch in self.minibatch(x_test_vector,y_test_vector,batch_size=self.batch_size):
            loss,accuracy = self.run_valid_iter(x_batch,y_batch)
            test_stats["test_loss"].append(loss)        
            test_stats["test_f1"].append(accuracy) 
        for key, value in test_stats.items():
            test_stats[key] = np.mean(value)
        return test_stats
    
    
    def run_train_iter(self,x_batch,y_batch):
        x_batch = x_batch.to(self.device)
        y_batch = y_batch.to(self.device)
        
        self.optim.zero_grad()
        out = self.model(x_batch)  # forward the data in the model
        loss = self.loss(out,y_batch)  # compute loss
        
        loss.backward()             # backpropagate to compute gradients for current iter loss

        self.optim.step()  # update network parameters

        if self.scheduler:
            self.scheduler.step()
        _, pred = torch.max(out.data, 1)  # get argmax of predictions

        groud_truth = y_batch.data.detach().cpu().numpy()
        prediction = pred.data.detach().cpu().numpy() 
        f1 = f1_score(groud_truth,prediction >0.5, average='micro')
        return loss.item(), f1
        
    def run_valid_iter(self,x_batch,y_batch):
        x_batch = x_batch.to(self.device)
        y_batch = y_batch.to(self.device)
        out = self.model(x_batch)  # forward the data in the model
        loss = self.loss(input=out, target=y_batch)  # compute loss

        _, pred = torch.max(out.data, 1)  # get argmax of predictions
        groud_truth = y_batch.data.detach().cpu().numpy()
        prediction = pred.data.detach().cpu().numpy() 
        f1 = f1_score(groud_truth,prediction >0.5, average='micro')
        return loss.item(), f1    

    def are_similar(self,sentence1,sentence2):
        """
        Function to test whether two senteces are similar; whether they belong to the same class

            ------
            Input
                sentence1: string represeting the first sentence    
                sentence2: string represeting the second sentence    
        """

        sent_list = self.feature_extractor.encode([sentence1,sentence2], tokenize=True)
        sent_list = self.pca_transform.transform(sent_list)
        class1 = torch.max(self.model(torch.Tensor(sent_list[0])).data,0)
        class2 = torch.max(self.model(torch.Tensor(sent_list[1])).data,0)
        if class1 == class2 :
            return True
        else:
            return False
